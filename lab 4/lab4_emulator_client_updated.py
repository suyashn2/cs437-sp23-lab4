# # Import SDK packages
# from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
# import time
# import json
# import pandas as pd
# import numpy as np

# #TODO 1: modify the following parameters
# #Starting and end index, modify this
# device_st = 0
# device_end = 5

# #Path to the dataset, modify this
# data_path = "./data2/vehicle{}.csv"

# #Path to your certificates, modify this
# certificate_formatter = "./section1_6_cert/cert{}.pem"
# key_formatter = "./section1_6_cert/private{}.key"


# class MQTTClient:
#     def __init__(self, device_id, cert, key):
#         # For certificate based connection
#         self.device_id = str(device_id)
#         self.state = 0
#         self.client = AWSIoTMQTTClient(self.device_id)
#         #TODO 2: modify your broker address
#         self.client.configureEndpoint("a2cnmv2ga7g15m-ats.iot.us-east-1.amazonaws.com", 8883)
#         self.client.configureCredentials("C:/Users/Suyash/Downloads/lab 4/certificates/AmazonRootCA1 (1).pem", key, cert)
#         self.client.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
#         self.client.configureDrainingFrequency(2)  # Draining: 2 Hz
#         self.client.configureConnectDisconnectTimeout(10)  # 10 sec
#         self.client.configureMQTTOperationTimeout(5)  # 5 sec
#         self.client.onMessage = self.customOnMessage
        

#     def customOnMessage(self,message):
#         #TODO3: fill in the function to show your received message
#         print("client {} received payload {} from topic {}".format(self.device_id, message.payload, message.topic))


#     # Suback callback
#     def customSubackCallback(self,mid, data):
#         #You don't need to write anything here
#         pass


#     # Puback callback
#     def customPubackCallback(self,mid):
#         #You don't need to write anything here
#         pass


#     def publish(self):
#         #TODO4: fill in this function for your publish
#         self.client.subscribeAsync("hello/world/pubsub", 0, ackCallback=self.customSubackCallback)
        
#         payload = {
# 			"device_id": self.device_id,
# 			"features": list(data[int(self.device_id)]["vehicle_CO"])
# 		}

#         self.client.publishAsync("hello/world/pubsub", json.dumps(payload), 0, ackCallback=self.customPubackCallback)



# print("Loading vehicle data...")
# data = []
# for i in range(5):
#     a = pd.read_csv(data_path.format(i))
#     df = pd.DataFrame(a, columns=["timestep_time","vehicle_CO","vehicle_CO2","vehicle_HC","vehicle_NOx","vehicle_PMx","vehicle_angle","vehicle_eclass",\
#                                  "vehicle_electricity","vehicle_fuel","vehicle_id","vehicle_lane","vehicle_noise","vehicle_pos","vehicle_route","vehicle_speed",\
#                                  "vehicle_type","vehicle_waiting","vehicle_x","vehicle_y"])
#     data.append(df)

# print("Initializing MQTTClients...")
# clients = []
# for device_id in range(device_st, device_end):
#     client = MQTTClient(device_id,certificate_formatter.format(device_id,device_id) ,key_formatter.format(device_id,device_id))
#     client.client.connect()
#     clients.append(client)
 

# while True:
#     print("send now?")
#     x = input()
#     if x == "s":
#         for i,c in enumerate(clients):
#             c.publish()

#     elif x == "d":
#         for c in clients:
#             c.client.disconnect()
#         print("All devices disconnected")
#         exit()
#     else:
#         print("wrong key pressed")

#     time.sleep(3)


# Import SDK packages
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import time
import json
import pandas as pd
import numpy as np


#TODO 1: modify the following parameters
#Path to the dataset, modify this
device_st = 0
device_end = 5

#Path to the dataset, modify this
data_path = "./data2/vehicle{}.csv"

#Path to your certificates, modify this
certificate_formatter = "./section1_6_cert/cert{}.pem"
key_formatter = "./section1_6_cert/private{}.key"


class MQTTClient:
    def __init__(self, device_id, cert, key):
        # For certificate based connection
        self.device_id = str(device_id)
        self.state = 0
        self.client = AWSIoTMQTTClient(self.device_id)
        #TODO 2: modify your broker address
        self.client.configureEndpoint("a2cnmv2ga7g15m-ats.iot.us-east-1.amazonaws.com", 8883)
        self.client.configureCredentials("C:/Users/Suyash/Downloads/lab 4/certificates/AmazonRootCA1 (1).pem", key, cert)
        self.client.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
        self.client.configureDrainingFrequency(2)  # Draining: 2 Hz
        self.client.configureConnectDisconnectTimeout(10)  # 10 sec
        self.client.configureMQTTOperationTimeout(5)  # 5 sec
        self.client.onMessage = self.customOnMessage
        

    def customOnMessage(self,message):
        #TODO3: fill in the function to show your received message
        print("client {} received payload {} from topic {}".format(self.device_id, message.payload, message.topic))


    # Suback callback
    def customSubackCallback(self,mid, data):
        #You don't need to write anything here
        pass


    # Puback callback
    def customPubackCallback(self,mid):
        #You don't need to write anything here
        pass


    def subscribe(self, Payload="payload"):
        #TODO4: fill in this function for your publish
        self.client.subscribeAsync("hello/world/pubsub", 0, ackCallback=self.customSubackCallback)

    def publish(self):
        
        payload = {
			"device_id": self.device_id,
			"features": list(data[int(self.device_id)]["vehicle_CO"])[0]
		}
        
        self.client.publishAsync("hello/world/pubsub", json.dumps(payload), 0, ackCallback=self.customPubackCallback)



print("Loading vehicle data...")
data = []
for i in range(5):
    a = pd.read_csv(data_path.format(i))
    data.append(a)

print("Initializing MQTTClients...")
clients = []
for device_id in range(device_st, device_end):
    client = MQTTClient(device_id,certificate_formatter.format(device_id,device_id) ,key_formatter.format(device_id,device_id))
    client.client.connect()
    clients.append(client)
 

while True:
    print("send now?")
    x = input()
    if x == "s":
        for i,c in enumerate(clients):
            if i > 3:
                c.publish()
            else:
                c.subscribe()

    elif x == "d":
        for c in clients:
            c.client.disconnect()
        print("All devices disconnected")
        exit()
    else:
        print("wrong key pressed")

    time.sleep(3)



