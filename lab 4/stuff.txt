pscp -pw raspberry 12e363a85dd62d900efb63c6f838f834efe4f39b428efa21e81fb92269018076-certificate.pem.crt pi@192.168.137.124:/home/pi
pscp -pw raspberry 12e363a85dd62d900efb63c6f838f834efe4f39b428efa21e81fb92269018076-public.pem.key pi@192.168.137.124:/home/pi
pscp -pw raspberry 12e363a85dd62d900efb63c6f838f834efe4f39b428efa21e81fb92269018076-private.pem.key pi@192.168.137.124:/home/pi
pscp -pw raspberry AmazonRootCA1.pem pi@192.168.137.124:/home/pi]



{
  "coreThing" : {
    "caPath": "AmazonRootCA1.pem",
    "certPath": "12e363a85dd62d900efb63c6f838f834efe4f39b428efa21e81fb92269018076-certificate.pem.crt",
    "keyPath": ""12e363a85dd62d900efb63c6f838f834efe4f39b428efa21e81fb92269018076-private.pem.key",
    "thingArn": "arn:aws:iot:us-east-1:485718026634:thing/MyGreengrassV1Core",
    "iotHost": "a2cnmv2ga7g15m-ats.iot.us-east-1.amazonaws.com",
    "ggHost": "greengrass-ats.iot.us-east-1.amazonaws.com",
    "keepAlive": 600
  },
  "runtime": {
    "cgroup": {
      "useSystemd": "yes"
    }
  },
  "managedRespawn": false,
  "crypto": {
    "caPath": "file:///greengrass/certs/AmazonRootCA1.pem",
    "principals": {
      "SecretsManager": {
        "privateKeyPath": "file:///greengrass/certs/12e363a85dd62d900efb63c6f838f834efe4f39b428efa21e81fb92269018076-private.pem.key"
      },
      "IoTCertificate": {
        "privateKeyPath": "file:///greengrass/certs/12e363a85dd62d900efb63c6f838f834efe4f39b428efa21e81fb92269018076-private.pem.key",
        "certificatePath": "file:///greengrass/certs/12e363a85dd62d900efb63c6f838f834efe4f39b428efa21e81fb92269018076-certificate.pem.crt"
      }
    }
  }
}





cd path-to-certs-folder
python basicDiscovery.py --endpoint a2cnmv2ga7g15m-ats.iot.us-east-1.amazonaws.com --rootCA "AmazonRootCA1 (1).pem" --cert 98dcc40cf809f0616765de8b54493a4246bcd400b6f453c8e78dd90b02daee96-certificate.pem.crt --key 98dcc40cf809f0616765de8b54493a4246bcd400b6f453c8e78dd90b02daee96-private.pem.key --thingName HelloWorld_Publisher --topic "hello/world/pubsub" --mode publish --message "Hello, World! Sent from HelloWorld_Publisher"



cd C:\Users\Suyash\Downloads\certificates
python basicDiscovery.py --endpoint a2cnmv2ga7g15m-ats.iot.us-east-1.amazonaws.com --rootCA "AmazonRootCA1 (3).pem" --cert 53f0707a68fcd05de1fa02e09b663cf71c9fa6b19f8554bf10425bdd76e87a76-certificate.pem.crt --key 53f0707a68fcd05de1fa02e09b663cf71c9fa6b19f8554bf10425bdd76e87a76-private.pem.key --thingName HelloWorld_Subscriber --topic 'hello/world/pubsub' --mode subscribe



cd C:\Users\Suyash\Downloads\GG_switch
python lightController.py --endpoint AWS_IOT_ENDPOINT --rootCA AmazonRootCA1.pem --cert switchCertId-certificate.pem.crt --key switchCertId-private.pem.key --thingName GG_TrafficLight --clientId GG_Switch


# devices talking with each other - pub-sub

python basicDiscovery.py --endpoint a2cnmv2ga7g15m-ats.iot.us-east-1.amazonaws.com --rootCA AmazonRootCA1.pem --cert cert0.pem --key private0.key --thingName lab4thing-0 --topic vehicle/info --mode publish --message "helo wolrd" --device 0


  python basicDiscovery.py --endpoint a2cnmv2ga7g15m-ats.iot.us-east-1.amazonaws.com --rootCA AmazonRootCA1.pem --cert cert1.pem --key private1.key --thingName lab4thing-1 --topic vehicle/info --mode subscribe --device 1



# device 0 sends data to lambda, and gets it back by subscribing

python basicDiscovery.py --endpoint a2cnmv2ga7g15m-ats.iot.us-east-1.amazonaws.com --rootCA AmazonRootCA1.pem --cert cert0.pem --key private0.key --thingName lab4thing-0 --topic vehicle/info --mode publish --message "helo wolrd" --device 0

python basicDiscovery.py --endpoint a2cnmv2ga7g15m-ats.iot.us-east-1.amazonaws.com --rootCA AmazonRootCA1.pem --cert cert0.pem --key private0.key --thingName lab4thing-0 --topic vehicle/max --mode subscribe --device 0

