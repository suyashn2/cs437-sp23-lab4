# Connecting to AWS
import boto3

import json
# Create random name for things
import random
import string

# Parameters for Thing
thingArn = ''
thingId = ''
thingName = ''.join(
    [random.choice(string.ascii_letters + string.digits) for n in range(15)])
defaultPolicyName = 'MyIotPolicy'
###################################################


def createThing(t, name):
    global thingClient
    thingResponse = thingClient.create_thing(
        thingName=name
    )
    print("created device{}".format(t))
    data = json.loads(json.dumps(thingResponse, sort_keys=False, indent=4))
    for element in data:
        if element == 'thingArn':
            thingArn = data['thingArn']
        elif element == 'thingId':
            thingId = data['thingId']

            createCertificate()


def createCertificate():
    global thingClient
    certResponse = thingClient.create_keys_and_certificate(
        setAsActive=True
    )
    data = json.loads(json.dumps(certResponse, sort_keys=False, indent=4))
    for element in data:
        if element == 'certificateArn':
            certificateArn = data['certificateArn']
        elif element == 'keyPair':
            PublicKey = data['keyPair']['PublicKey']
            PrivateKey = data['keyPair']['PrivateKey']
        elif element == 'certificatePem':
            certificatePem = data['certificatePem']
        elif element == 'certificateId':
            certificateId = data['certificateId']

    with open('public{}.key'.format(t), 'w') as outfile:
        outfile.write(PublicKey)
    with open('private{}.key'.format(t), 'w') as outfile:
        outfile.write(PrivateKey)
    with open('cert{}.pem'.format(t), 'w') as outfile:
        outfile.write(certificatePem)

    response = thingClient.attach_policy(
        policyName=defaultPolicyName,
        target=certificateArn
    )
    response = thingClient.attach_thing_principal(
        thingName=thingName,
        principal=certificateArn
    )


# todo
thingClient = boto3.client('iot', 
                           aws_access_key_id='ASIAXCFYM5GFKGSZPPCA',
                           aws_secret_access_key='mL9IZMitFJvEwMgjHImbsYxvsDmpTrQS7Jph1J8G', 
                           aws_session_token='IQoJb3JpZ2luX2VjEO7//////////wEaCXVzLWVhc3QtMSJHMEUCIHugati3aRzEZ+ndbVgZCDxbODbG+e9qETxL4PAGfqveAiEAmPPnY53cW4pSBowDs2lITiuZqCdRzaBzouDDKuwRTukq8wIIxv//////////ARAAGgw0ODU3MTgwMjY2MzQiDGyOixfzXtV2nMb6TirHAhA2kgoME2Us6i45pR5dZMjtfjLX3rbsfJI6tW7h9l8iNAm/CcJ51vOjngyg0uw15hGN+kkNLr0QBmgSV/nY0Jh0urlSP7gNjokXmH/jca9+A1dmWDCH7p/1Gn72WtONSQvp8syRVeWNMaT1Pdp/n+47f+KeC2smC3Uq5a/tmCSspj1Q/Bsni0iOJckNEjBt9Km1qRYIDbEmEW4crJ1P8JSxh4NuvbovOTGKfYJOZNm22musvvBw7qPJlV+s8aPzqZlmXMct3XRFRR358IEqDEQ+t1XkykwbXx/iLZ7WPV+gluiXcOh0rWAKEgC++QFJ7rpWkFLsxh1PMAeT2ZyUmg0DFg8U9f1QNr2BO4IIXXy5EtgLCCsEo+thshcwcRzFRZjefLo/WRMpeS5UcG/UDsdAl+5pZkIjsvoU9zm7M7A/I9IlTHpN7DDS26ehBjqnAdzwcRHrEIB1d66CmzHLUlq1GqwhTPGZ/e5cFSEGbr9yOeI8rVJZRMpXSsIGRBowJ2Po9UWknvJ1t0pZMveRzTs4Q2tn3ZOnl9bnPcoJcIBeXOHdMLzyquw5MF5TaCALJexaGGKynty20+xi8Oh7UtCfvO61ej3bIZN+aHlfzfBh1yWMy5YUBxT3RUPbsERfaWYa18fBHxR5snj/LhEgggreHOlbqtVI', 
                           region_name='us-east-1')

if __name__ == "__main__":
	for t in range(5):
		thingName = 'lab4thing-' + str(t)
		createThing(t, thingName)
